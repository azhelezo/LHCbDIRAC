[v10.4.23]
*BKK

FIX: (!1309) TDET is not a real detector so ignore it when generating bkk path



[v10.4.22]
*ProductionManagement

FIX: (!1298) Adding custom tags to Analysis Productions
NEW: (!1295) Add option to create file types to dirac-production-request-submit



[v10.4.21]
*ProductionManagement

CHANGE: (!1289) NotifyAgent: do not send by default email for status changes
NEW: (!1275) HistogramMergingAgent to merge, upload and register histograms from test productions, then commit metadata to a git repo consumed by Monet



[v10.4.20]
*tests

FIX: (!1274) replace rootPath in testJobDefinitions

*ProductionManagement

NEW: (!1270) dirac-production-request-run-local supports productions with input data



[v10.4.19]
*ProductionManagement

NEW: (!1265) Support submitting Analysis Productions with dirac-production-request-submit
FIX: (!1263) Compatibility with LHCbWebDIRAC for YAML based productions



[v10.4.18]
[v10.4.17]
*ResourceStatusSystem

FIX: (!1251) Fixed path to topology.json file

*BKK

FIX: (!1246) _getDataQuality does not raise an error if the DQ is not set

Thank you for writing the text to appear in the release notes. It will show up
exactly as it appears between the two bold lines

*TransformationSystem

FIX: (!1228) the script was not expecting log files within a zip file but a flat directory... This is fixed in this MR



[v10.4.16]
*ProductionManagement

FIX: (!1244) Filter out invalid results from BookkeepingClient.getSimulationCondition when submitting productions

*Bookkeeping

FIX: (!1243) Handling errors in XMLFilesReaderManager for production jobs with invalid XML
CHANGE: (!1241) Factorise bookkeeping XML generation out of BookkeepingReport



[v10.4.15]
*ProductionManagement

NEW: (!1235) dirac-production-request-run-local command for locally testing YAML-defined production requests
NEW: (!1226) Add dirac-production-submit command for creating production requests from a YAML file

*ResourceStatusSystem

CHANGE: (!1229) NagiosTopologyAgent does not generate XML topology file anymore



[v10.4.14]
*Bookkeeping

FIX: (!1225) Use bind variables when querying `steps.OptionFiles`

*ProductionManagement

FEAT: (!1225) Support lbexec for PyConf based applications
FIX: (!1223) KeyError when submitting productions with invalid initial state



[v10.4.13]
*BKK

FIX: (!1217) bkk MC test is a bit less strict about timestamp comparison

*TransformationSystem

FIX: (!1212) skip jobs that did not produce events

*DataManagementSystem

FIX: (!1210) typo in finding the corresponding site of an SE in LHCbFTS3Plugin



[v10.4.12]
*WorkloadManagementSystem

CHANGE: (!1207) Ensure stderr from applications is always kept
FIX: (!1207) Reduce memory usage in `RunApplication` by not storing stdout/stderr in memory

*DataManagementSystem

FIX: (!1203) treat the case when the `['Failed']` field returned by BK is a list and not a dict

*Core

FIX: (!1203) handle weird list of paths in `dirac-loop`

*TransformationSystem

CHANGE: (!1203) add option in `dirac-transformation-add-files` to allow resetting files already present as `Unused`. This is particularly useful in DM transformations as files may be considered more than once for replication or removal

*Production

CHANGE: (!1202) Do not get PFN for tape SEs



[v10.4.11]


[v10.4.10]
[v10.4.9]
*ProductionManagement

FIX: (!1195) Issues found while processing FEST data

*DataManagementSystem

FIX: (!1194) Strip "mdf:" prefix from URLs in metalink files

*Bookkeeping

CHANGE: (!1192) Condition string generation simplified, and sub-detector status not stored in the DB anymore



[v10.4.8]
*tests

FIX: (!1181) remove a space in test_UserJobs

*Bookkeeping

NEW: (!1178) Support using bind variabes when querying oracle databases
NEW: (!1178) Add `getProductionFilesBulk` method

*ProductionManagement

NEW: (!1178) Add APSyncAgent for populating AnalysisProductionsDB
NEW: (!1178) Support removing transformations for the AnalysisProductionsDB cache
FIX: (!1178) Dictionaries with integer keys in AnalysisProductions
FIX: (!1178) Don't create an AnalysisProductionsDB engine for each RPC call



[v10.4.7]
Please follow the template:

*Dashboards

NEW: (!1173) Added a new WMSDashboard for LHCbDIRAC

*Core

FIX: (!1172) Return stdout/stderr in RunApplication._runApp

*Bookkeeping

FIX: (!1168) fix dirac_bookkeeping_decays_path to run on real data

*ProductionManagement

FIX: (!1167) Getting production request submission templates
NEW: (!1158) AnalysisProductions service for keeping metadata about Analysis Productions

*TransformationSystem

NEW: (!1164) When running large productions or large DMS transformations, The production or data manager has to continuously monitor the status of the transformations and increment e.g. the end run of the data range in order to keep a reasonable number of Waiting jobs or running replication requests. This new feature allows with a new `TransformationPlugin` parameter `ThrottlePendingTasks` to limit the number of pending tasks targeting a given storage element (i.e. as source for jobs or as destination for replications or removals).In order to not overload the TS, one should set the `Period` parameter to a reasonable value. By default if `ThrottlePendingTasks` is set, there is a minimum period of 6 hours between each running of the plugin but this can be altered using the script below...
CHANGE: (!1164) factorize some of the checks that were done in `TransformationPlugin` to move them to `PluginUtilities` to improve readability / maintainability(?)
NEW: (!1164) new script `dirac-transformation-add-parameter` allows to add a parameter to an existing transformation
FIX: (!1164) in several scripts use the `getTransformations()` method for parsing (list) of transformations passed as arguments

*DataManagementSystem

FIX: (!1164) fix `dirac-dms-add-file` as no registration error was reported

*All

FIX: (!1164) remove many `from __future__ import ` in scripts (not all!)



[v10.4.6]
*CI

CHANGE: (!1165) Deploy job compatible with python3 versions
CHANGE: (!1165) pre-commit more permissive with python3 versions



[v10.4.5]
Very minor changes, except an important fix for setting the DQ flag as it was crashing

*TransformationSystem

CHANGE: (!1159) DataRecoveryAgent sets files to Processed in case they have descendents



[v10.4.4]
[v10.4.1]
[v10.4.0]
