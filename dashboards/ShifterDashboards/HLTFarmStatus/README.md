# HLTFarm status
The `HLTFarm Status` dashboard can be found in the [Shifter Overview](https://monit-grafana.cern.ch/d/gqRdVq0Vz/hltfarm-status) folder of the LHCb grafana organisation.

## Jobs by Status
Using WMS indices as datasource

Note: Only visually validated ([issue](https://github.com/DIRACGrid/WebAppDIRAC/issues/722) blocking validation)


## Failed or Rescheduled Jobs by FinalMajorStatus
Using AccountingDB as datasource

Note: Validated by comparing averages

## Failed Jobs by FinalMinorStatus
Using AccountingDB as datasource

Note: Validated by comparing averages
